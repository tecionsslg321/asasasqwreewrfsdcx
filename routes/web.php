<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ManageFormController@GetForm')->name('frotend.form.view');
Route::get('/all', 'ManageFormController@AllGetForm')->name('frotend.form.all');
Route::get('/edit/{id}', 'ManageFormController@EditForm')->name('frotend.form.edit');
Route::post('/edit', 'ManageFormController@Updatedata')->name('frotend.form.update');
Route::get('/delete/{id}', 'ManageFormController@dateteData')->name('frotend.form.datete');
Route::post('/', 'ManageFormController@FormPost')->name('frotend.form.post');
