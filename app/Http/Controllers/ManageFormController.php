<?php

namespace App\Http\Controllers;

use App\FormModel;
use Illuminate\Http\Request;

class ManageFormController extends Controller
{
    public function GetForm(){

        $b = FormModel::all();

        return view('test.form')->with([
            'form' => $b
        ]);


    }

    public function AllGetForm(){

        $b = FormModel::all();

        return view('test.table')->with([
            'form' => $b
        ]);


    }

    public function FormPost(Request $request){

        $a = new FormModel();
        $a->title = $request->title;
        $a->description = $request->description;
        $a->save();


        return redirect()->route('frotend.form.view');



    }


    public function EditForm($id){

        $a =  FormModel::find($id);


        return view('test.edit')->with([
            'form' => $a
        ]);




    }


    public function Updatedata(Request $request){
        $a =  FormModel::find($request->_formid);
        $a->title = $request->title;
        $a->description = $request->description;
        $a->save();
       return redirect()->route('frotend.form.view');

    }


    public function dateteData($id){
        $a =  FormModel::find($id);

        $a->delete();

        return redirect()->route('frotend.form.view');

    }


}
